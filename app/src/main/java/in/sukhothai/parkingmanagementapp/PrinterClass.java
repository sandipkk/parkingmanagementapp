package in.sukhothai.parkingmanagementapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;

public class PrinterClass {
    private static Printer mPrinter = null;
    Context context ;
    public boolean createReceiptData() {
        String method = "";
        Bitmap logoData = BitmapFactory.decodeFile(String.valueOf(R.drawable.logo_parking));
        StringBuilder textData = new StringBuilder();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;

        if (mPrinter == null) {
            return false;
        }

        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);

            method = "addImage";
            mPrinter.addImage(logoData, 0, 0,
                    logoData.getWidth(),
                    logoData.getHeight(),
                    Printer.COLOR_1,
                    Printer.MODE_MONO,
                    Printer.HALFTONE_DITHER,
                    Printer.PARAM_DEFAULT,
                    Printer.COMPRESS_AUTO);

            method = "addFeedLine";
            mPrinter.addFeedLine(1);

            textData.append("Aug, 2021 12:06 PM           SRN #0000022\n");
            //    textData.append("Aug, 2021 12:06 PM",0,18);
            textData.append("------------------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            method = "addText";

            mPrinter.addText("Vehicle Number : 3456\n");
            mPrinter.addText("\n");
            method = "addText";
            mPrinter.addTextSize(1, 1);
            mPrinter.addText("Entry         Exit         Hours\n");
            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addText("12:04PM       12:04PM        25   \n");
            textData.append("    ------------------------------------------\n");
            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("      Total Amount : Rs. 1500\n");
            textData.append("    ------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            method = "addText";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addText("Terms of Parking\n");
            mPrinter.addText(textData.toString());
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("* Parking will be at Owner's risk\n");
            textData.append("* No responsibility of any loss of valuables\n");
            textData.append("* No responsibility for any vehicle damage\n");
            textData.append("* by other vehicle or accident\n");
            textData.append("* Any chargable timing above an hour will be  \n  consider as next hour\n");
            textData.append("* Operating Hours 7 AM - 11 AM\n");

            textData.append("    ------------------------------------------\n");

            method = "addText";
            mPrinter.addText(textData.toString());

            textData.delete(0, textData.length());
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addFeedLine";
            mPrinter.addFeedLine(2);

            method = "addBarcode";

            mPrinter.addBarcode("01209457",
                    Printer.BARCODE_CODE39,
                    Printer.HRI_BELOW,
                    Printer.FONT_A,
                    barcodeWidth,
                    barcodeHeight);
         /*   method = "addText";
            mPrinter.addText("Thank You");*/
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            method = "addText";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addText("Thank You\n");
            mPrinter.addText(textData.toString());
            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            return false;
        }

        textData = null;
        return true;
    }

/*    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        if (!isPrintable(status)) {

            Log.i("#########", "makeErrorMessage");
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {

            Log.i("#########", "sendData");
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return false;
        }

        return true;
    }*/

    private boolean initializeObject() {
        try {

        //    mPrinter = new Printer(Printer.TM_M30, Printer.MODEL_ANK, getActivity());

        } catch (Exception e) {

            Log.i("#########", "Printer");
            return false;
        }

  //      mPrinter.setReceiveEventListener(this);

        return true;
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }
        mPrinter.clearCommandBuffer();
        mPrinter.setReceiveEventListener(null);
        mPrinter = null;
    }

    private boolean connectPrinter() {
        boolean isBeginTransaction = false;

        if (mPrinter == null) {
            return false;
        }

        try {

            String printerIp = "172.16.5.16";

            Log.i("IP######", printerIp);

            mPrinter.connect("TCP:" + printerIp, Printer.PARAM_DEFAULT);
            Log.i("#########", "connect");

        } catch (Exception e) {

            Log.i("#########", "connectERROR");
            e.printStackTrace();
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
            Log.i("#########", "beginTransaction");

        } catch (Exception e) {
            Log.i("#########", "beginTransactionERROR");
        }

        if (isBeginTransaction == false) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        return true;
    }
/*
    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    Log.i("#########", "endTransaction");
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    Log.i("########", "disconnect");
                }
            });
        }

        finalizeObject();
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {

                Log.i("########", "PRINT SUCCESSFULLY!!");

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                        Log.i("########", "disconnectPRINTER");
                    }
                }).start();
            }
        });
    }*/
}
