package in.sukhothai.parkingmanagementapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.provider.Settings;
import android.text.Html;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;
import in.sukhothai.parkingmanagementapp.Utilities.IOUtils;
import in.sukhothai.parkingmanagementapp.Utilities.LocationUpdatesService;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;
import in.sukhothai.parkingmanagementapp.Utilities.SharedPrefUtil;
import in.sukhothai.parkingmanagementapp.Utilities.SlidingTab.SlidingTabLayout;
import in.sukhothai.parkingmanagementapp.network.NetworkUtils;
import in.sukhothai.parkingmanagementapp.network.NetworkUtilsReceiver;


public class MainActivity extends AppCompatActivity implements NetworkUtilsReceiver.NetworkResponseInt {
    private String TAG = this.getClass().getSimpleName();
    private RelativeLayout mainRelLayout;
    private SlidingTabLayout tourTabs;
    public static ViewPager tourPager;
    private NetworkUtilsReceiver networkUtilsReceiver;
    boolean doubleBackToExitPressedOnce = false;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {

            setContentView(R.layout.activity_main);
            /*Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            setTitle("Parking Management");*/
            MainActivity.this.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            //TODO Register receiver
            networkUtilsReceiver = new NetworkUtilsReceiver(this);
            registerReceiver(networkUtilsReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

            //TODO Check Run Time Permission
            if (Build.VERSION.SDK_INT >= 23) {
                TedPermission.with(MainActivity.this)
                        .setPermissionListener(permissionlistener)
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .check();
            } else {
                startLocationServices(MainActivity.this);
            }

            if(isTimeAutomatic(MainActivity.this)) {

            } else {
                startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
                Toast.makeText(this, "Please Set Your Time and Date Settings to Auto", Toast.LENGTH_SHORT).show();
                //something else
            }

            mainRelLayout = (RelativeLayout) findViewById(R.id.mainRelLayout);
            tourTabs = (SlidingTabLayout) findViewById(R.id.tourTabSlider);
            tourPager = (ViewPager) findViewById(R.id.tourTabsPager);
            tourPager.setAdapter(new viewPagerAdapter(getSupportFragmentManager()));
            tourTabs.setViewPager(tourPager);
            tourTabs.setSelectedIndicatorColors(ContextCompat.getColor(MainActivity.this, R.color.bluegray100));
            tourPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    try {
                        if (position == 1) {
                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(mainRelLayout.getWindowToken(), 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            tourTabs = (SlidingTabLayout) findViewById(R.id.tourTabSlider);
            tourPager = (ViewPager) findViewById(R.id.tourTabsPager);
            tourPager.setAdapter(new viewPagerAdapter(getSupportFragmentManager()));
            tourTabs.setViewPager(tourPager);
            tourTabs.setSelectedIndicatorColors(ContextCompat.getColor(MainActivity.this, R.color.bluegray100));

            tourPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    try {

                        if (position == 1) {
                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(mainRelLayout.getWindowToken(), 0);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class viewPagerAdapter extends FragmentPagerAdapter {
        String tabs[];


        public viewPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.addViewTabsName);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment f = null;

            switch (position) {
                case 0: {
                    f = new AddVehicleFragment();

                    break;
                }
                case 1: {
                    f = new ViewParkingVehiclesFragment();
                    break;
                }
            }
            return f;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {

            try {

                startLocationServices(MainActivity.this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            Toast.makeText(MainActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
        }
    };

    //TODO Location Services Enable
    public void startLocationServices(Context context) {

        try {

            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                IOUtils.buildAlertMessageNoGps(MainActivity.this);

            } else {

                if (!IOUtils.isServiceRunning(LocationUpdatesService.class, context)) {
                    // LOCATION SERVICE
                    startService(new Intent(context, LocationUpdatesService.class));
                    MyLog.e(TAG, "Location service is already running");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            switch (requestCode) {
                case 01:
                    switch (resultCode) {
                        case Activity.RESULT_OK:
                            // All required changes were successfully made
                            if (!IOUtils.isServiceRunning(LocationUpdatesService.class, MainActivity.this)) {
                                // LOCATION SERVICE
                                startService(new Intent(MainActivity.this, LocationUpdatesService.class));
                                MyLog.e(TAG, "Location service is already running");
                            }
                            break;
                        case Activity.RESULT_CANCELED:
                            // The user was asked to change settings, but chose not to
                            break;
                        default:
                            break;
                    }
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
            MyLog.i("Error", e.getMessage());
        }
    }

    //TODO Check Network Availability
    @Override
    public void NetworkOpen() {

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void NetworkClose() {

        try {

            if (!NetworkUtils.isNetworkConnectionOn(MainActivity.this)) {

                Snackbar snackbar = Snackbar
                        .make(mainRelLayout, getResources().getString(R.string.error_message_no_internet), Snackbar.LENGTH_LONG)
                        .setAction(getResources().getString(R.string.settings), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                            }
                        });

                snackbar.setActionTextColor(Color.RED);
                snackbar.show();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return android.provider.Settings.System.getInt(c.getContentResolver(), android.provider.Settings.System.AUTO_TIME, 0) == 1;
        }
    }

}