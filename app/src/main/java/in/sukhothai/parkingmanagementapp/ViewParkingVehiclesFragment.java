package in.sukhothai.parkingmanagementapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dantsu.escposprinter.connection.DeviceConnection;
import com.dantsu.escposprinter.connection.tcp.TcpConnection;
import com.dantsu.escposprinter.textparser.PrinterTextParserImg;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import in.sukhothai.parkingmanagementapp.POJO.ParkListAdapter;
import in.sukhothai.parkingmanagementapp.POJO.ParkingResponse;
import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDaoList;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingList;
import in.sukhothai.parkingmanagementapp.Utilities.Constants;
import in.sukhothai.parkingmanagementapp.Utilities.IOUtils;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;
import in.sukhothai.parkingmanagementapp.async.AsyncEscPosPrinter;
import in.sukhothai.parkingmanagementapp.async.AsyncTcpEscPosPrint;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import mehdi.sakout.fancybuttons.FancyButton;

public class ViewParkingVehiclesFragment extends Fragment {

    private static final String TAG = "FirstFragment";
    public static Context ctx;
    public static TextView valueTV;
    private RelativeLayout parkinglistRelativeLayout;
    public static Gson gson;
    private static List<ParkingList> ParkingDbList;
    private static List<BillingDaoList> billingDaoLists;
    public static RecyclerView RecyclerView;
    private static ParkingDataBase myDatabase;

    LinearLayoutManager linearLayoutManager;
    public static ParkListAdapter parkListAdapter;
    public static ParkingResponse parkingResponse;
    private IOUtils ioUtils;
    public static String billAmount = "", vehicleNumber = "", selectedDate = "";
    public static String parkInTM = "", parkOutTM = "", inDay = "";
    public static String outDay = "", parkingInDt = "", parkingOutDt = "";
    public static String headerDate = "", srno = "", sgstAmount = "";
    public static String cgstAmount = "", parkingHours = "";
    public static String baseAmount = "", parkingType = "";
    public static ProgressBar progressBar;
    public static RelativeLayout relativeLayout;
    public static Dialog dialogTherapy;
    private ImageView closeHolderIMG;
    private TextInputEditText vehicleNumberET;
    private LinearLayout dateHolder;
    private FancyButton dateFB, searchBTN;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_first, container, false);
        try {
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relativelay_reclview);
            parkinglistRelativeLayout = view.findViewById(R.id.parkinglistRelativeLayout);
            RecyclerView = view.findViewById(R.id.RecyclerView);
            RecyclerView.setHasFixedSize(true);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
            RecyclerView.setLayoutManager(linearLayoutManager);
            myDatabase = Room.databaseBuilder(getActivity(), ParkingDataBase.class, "ParkingDb").allowMainThreadQueries().build();
            ctx = getActivity();
            setHasOptionsMenu(true);
            ioUtils = new IOUtils();
            gson = new Gson();
            //  getParkingList("", "");
            getDatafromdatabase();
            final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.container);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    //            getParkingList("", "");
                    getDatafromdatabase();
                    swipeRefreshLayout.setRefreshing(false);
                    exportJSON();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();

        }
        return view;
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
/*
    public static void getParkingList(String vehicleNumber, String date) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            IOUtils utils = new IOUtils();
            String JsonLink = Constants.parkingList + vehicleNumber + "&date=" + date;
            MyLog.i(TAG + " url@@@@", JsonLink);

            utils.getCommonStringRequest(Constants.GET, TAG, ctx, JsonLink, new IOUtils.VolleyCallback() {
                @Override
                public void onSuccess(String result) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        MyLog.i(TAG + "getParkingList#####", result);
                        JSONObject jsonObject = new JSONObject(result);
                        valueTV = new TextView(ctx);

                        if (jsonObject.has("data")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            if (jsonArray.toString().equalsIgnoreCase("[]")) {

                                RecyclerView.setVisibility(View.GONE);
                                relativeLayout.setVisibility(View.VISIBLE);
                                relativeLayout.removeAllViews();
                                valueTV.setText("No data available!");
                                valueTV.setGravity(Gravity.CENTER);
                                valueTV.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                                ((RelativeLayout) relativeLayout).addView(valueTV);
                            } else {

                                relativeLayout.setVisibility(View.GONE);
                                RecyclerView.setVisibility(View.VISIBLE);
                                parkingResponse = gson.fromJson(result, ParkingResponse.class);
                                parkListAdapter = new ParkListAdapter(parkingResponse.getData(), ctx);
                                RecyclerView.setAdapter(parkListAdapter);
                                parkListAdapter.notifyDataSetChanged();
                                RecyclerView.scheduleLayoutAnimation();
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        progressBar.setVisibility(View.GONE);
                    }
                }

            }, new IOUtils.VolleyFailureCallback() {
                @Override
                public void onFailure(String result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }
    }
    */

    public static void showFinalPrice(String tranNo) {

        try {

            String url = Constants.parkingOut;
            JSONObject jsonObject = new JSONObject();
            IOUtils ioUtils1 = new IOUtils();

            try {
                jsonObject.put("tranNo", Integer.parseInt(tranNo));

                MyLog.d(TAG + "JSON CREATED@@@", jsonObject.toString());
                View view = null;
                ioUtils1.sendCommonJSONObjectRequest(Constants.POST, TAG, ctx, url, jsonObject, new IOUtils.VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        MyLog.d(TAG + "showFinalPrice@@@", result.toString());
                        showDialogBox(view, result);
                    }
                }, new IOUtils.VolleyFailureCallback() {
                    @Override
                    public void onFailure(String result) {

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showDialogBox(View V, String result) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            LayoutInflater li = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View invetorydailogBox = li.inflate(R.layout.checkoutdailogbox, null);
            builder.setView(invetorydailogBox);
            AlertDialog dialog = builder.create();
            dialog.show();
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            try {
                JSONObject Finalresult = new JSONObject(result);
                MyLog.d("Finalresult@@PRINT", Finalresult.toString());
                JSONObject data = Finalresult.getJSONObject("data");
                billAmount = data.getString("billAmount");
                vehicleNumber = data.getString("vehicleNumber");
                parkInTM = data.getString("parkInTM");
                parkOutTM = data.getString("parkOutTM");
                outDay = data.getString("outDay");
                inDay = data.getString("inDay");
                parkingInDt = data.getString("parkingInDt");
                parkingOutDt = data.getString("parkingOutDt");
                headerDate = data.getString("headerDate");
                srno = data.getString("srno");
                sgstAmount = data.getString("sgstAmount");
                cgstAmount = data.getString("cgstAmount");
                parkingHours = data.getString("parkingHours");
                baseAmount = data.getString("baseAmount");
                parkingType = data.getString("parkingType");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            TextView vehicleNoTV = invetorydailogBox.findViewById(R.id.vehicleNoTV);
            //TextView totalHoursTV = invetorydailogBox.findViewById(R.id.totalHoursTV);
            TextView adDEDT = invetorydailogBox.findViewById(R.id.dailogvechicalEarkingprice);
            TextView ok = invetorydailogBox.findViewById(R.id.dailogok);
            vehicleNoTV.setText(vehicleNumber);
            //vehicleNoTV.setText(parkingHours);
            adDEDT.setText(ctx.getResources().getString(R.string.rs) + billAmount);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    if (parkingType.equalsIgnoreCase("Over Night")) {
                        printRecieptDataoutOverNight();
                    } else {
                        printRecieptDataout();
                    }
                    //    getParkingList("", "");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printRecieptDataout() {
        try {
            try {
                new AsyncTcpEscPosPrint(ctx)
                        .execute(getAsyncEscPosPrinterOut(new TcpConnection("172.16.5.16", 9100)));
            } catch (NumberFormatException e) {
                new androidx.appcompat.app.AlertDialog.Builder(ctx)
                        .setTitle("Invalid TCP port address")
                        .setMessage("Port field must be a number.")
                        .show();
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static void printRecieptDataoutOverNight() {
        try {
            try {
                new AsyncTcpEscPosPrint(ctx)
                        .execute(getAsyncEscPosPrinterOutOverNight(new TcpConnection("172.16.5.16", 9100)));
            } catch (NumberFormatException e) {
                new androidx.appcompat.app.AlertDialog.Builder(ctx)
                        .setTitle("Invalid TCP port address")
                        .setMessage("Port field must be a number.")
                        .show();
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static AsyncEscPosPrinter getAsyncEscPosPrinterOut(DeviceConnection printerConnection) {
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 203, 77f, 45);
        return printer.setTextToPrint(
                "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, ctx.getResources().getDrawableForDensity(R.drawable.newlogo, DisplayMetrics.DENSITY_MEDIUM)) + "</img>\n\n" +
                        "[L]" + headerDate + "[R][C]" + "SR#" + srno + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[C]<font size='tall'>  Vehicle Number :" + vehicleNumber + "</font>\n" +
                        "[C]\n" +
                        "[L]ENTRY[C][C]EXIT[R][C]HOURS\n" +
                        "[L]<font size='tall'>" + parkInTM + "</font>[C][C]<font size='tall'>" + parkOutTM + "</font>[R][C]<font size='tall'>" + parkingHours + "</font>\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]Parking Charges[R]Rs." + baseAmount + "\n" +
                        "[L]C.G.S.T @ 9%[R]Rs. " + cgstAmount + "\n" +
                        "[L]S.G.S.T @ 9%[R]Rs. " + sgstAmount + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<b><font size='big'>Total[R] Rs." + billAmount + "</font></b>\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='normal'>Sukhothai India Pvt.Ltd. <b>GST # 27AAOCS623OP2ZM</b></font>\n" +
                        "[C]<b><font size='normal'>Terms of Parking</b>\n" +
                        "[L]* Parking will be at owner's risk.\n" +
                        "* No responsibility of any loss of valuables.\n" +
                        "* No responsibility for any vehicle damage by     other vehicles or by accident.\n" +
                        "* Any chargable timing above an hour will be      consider as next hour.\n" +
                        "* Operating Hours 7 AM - 11 PM'\n</font>\n" +
                        "[C]<barcode type='ean8' height='10' text='none'>" + srno + "</barcode>\n" +
                        //   "[C]<barcode type='ean8' height='25' width='50' text='none'>"+srno+"</barcode>\n"+
                        "[C]<font size='normal'>Thank You</font>\n" +
                        "[C]\n" +
                        "[C]\n"
        );
    }

    public static AsyncEscPosPrinter getAsyncEscPosPrinterOutOverNight(DeviceConnection printerConnection) {
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 203, 77f, 45);
        return printer.setTextToPrint(
                "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, ctx.getResources().getDrawableForDensity(R.drawable.newlogo, DisplayMetrics.DENSITY_MEDIUM)) + "</img>\n\n" +
                        "[L]" + headerDate + "[R][C]" + "SR#" + srno + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[C]<font size='tall'>  Vehicle Number :" + vehicleNumber + "</font>\n" +
                        "[C]\n" +
                        "[R]ENTRY[C][L]EXIT\n" +
                        "[C]" + inDay + " " + parkingInDt + "       " + outDay + parkingOutDt + "\n" +
                        "[C]<font size='tall'>" + "  " + parkInTM + "</font>" + "            " + "<font size='tall'>" + parkOutTM + "</font>\n" +
                        "[C](Night Parking)\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]Parking Charges[R]Rs." + baseAmount + "\n" +
                        "[L]C.G.S.T @ 9%[R]Rs. " + cgstAmount + "\n" +
                        "[L]S.G.S.T @ 9%[R]Rs. " + sgstAmount + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<b><font size='big'>Total[R] Rs." + billAmount + "</font></b>\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='normal'>Sukhothai India Pvt.Ltd. <b>GST # 27AAOCS623OP2ZM</b></font>\n" +
                        "[C]<b><font size='normal'>Terms of Parking</b>\n" +
                        "[L]* Parking will be at owner's risk.\n" +
                        "* No responsibility of any loss of valuables.\n" +
                        "* No responsibility for any vehicle damage by     other vehicles or by accident.\n" +
                        "* Any chargable timing above an hour will be      consider as next hour.\n" +
                        "* Operating Hours 7 AM - 11 PM'\n</font>\n" +
                        "[C]<barcode type='ean8' height='10' text='none'>" + srno + "</barcode>\n" +
                        //   "[C]<barcode type='ean8' height='25' width='50' text='none'>"+srno+"</barcode>\n"+
                        "[C]<font size='normal'>Thank You</font>\n" +
                        "[C]\n" +
                        "[C]\n"
        );
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        MenuInflater inflater1 = getActivity().getMenuInflater();
        inflater1.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                showDialogBox(getActivity());
                break;
        }
        return true;
    }

    public void showDialogBox(Context context) {

        try {

            dialogTherapy = new Dialog(context);
            dialogTherapy.setContentView(R.layout.searchvehicledialogbox);
            dialogTherapy.setCancelable(false);
            closeHolderIMG = (ImageView) dialogTherapy.findViewById(R.id.closeHolderIMG);
            vehicleNumberET = (TextInputEditText) dialogTherapy.findViewById(R.id.vehicleNumberET);
            dateHolder = (LinearLayout) dialogTherapy.findViewById(R.id.dateHolder);
            dateFB = (FancyButton) dialogTherapy.findViewById(R.id.dateFB);
            searchBTN = (FancyButton) dialogTherapy.findViewById(R.id.searchBTN);

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("UTC"));
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            final DatePickerDialog datePickerDialog = new DatePickerDialog(
                    getActivity(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                    try {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
                        selectedDate = format1.format(calendar.getTime());
                        dateFB.setText(selectedDate);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, mYear, mMonth, mDay);

            dateHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        datePickerDialog.setTitle("Select date");
                        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                        datePickerDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            closeHolderIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        dialogTherapy.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            searchBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        dialogTherapy.dismiss();
                        String vehicleNo = vehicleNumberET.getText().toString();
                        //       getParkingList(vehicleNo, selectedDate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialogTherapy.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            dialogTherapy.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogTherapy.show();

            dialogTherapy.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        MyLog.i("@@@@@@@BACK", "PRESSED");
                        dialogTherapy.dismiss();
                        return true;
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getDatafromdatabase() {
        try {
            class GetData extends AsyncTask<Void, Void, List<ParkingList>> {

                @Override
                protected List<ParkingList> doInBackground(Void... voids) {
                    ParkingDbList = ViewParkingVehiclesFragment.myDatabase.parkingDao().getMyData();
                    return ParkingDbList;
                }

                @Override
                protected void onPostExecute(List<ParkingList> myDataList) {
                    //        adapter = new MyAdapter(myDataList, context);
                    //      rv.setAdapter(adapter);
                    //   adapter.notifyDataSetChanged();
                    parkListAdapter = new ParkListAdapter(ParkingDbList, billingDaoLists, ctx);
                    RecyclerView.setAdapter(parkListAdapter);
                    parkListAdapter.notifyDataSetChanged();
                    RecyclerView.scheduleLayoutAnimation();
                    super.onPostExecute(myDataList);
                }
            }
            GetData gd = new GetData();
            gd.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void exportJSON(){
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                // stdDatabase.stdDAO().getStudents();
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onComplete() {
                        ParkingDbList = myDatabase.parkingDao().getMyData();
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<ParkingList>>(){}.getType();
                        String stdJson = gson.toJson(ParkingDbList, type);
MyLog.e("stdJson",stdJson.toString());
                        // Call function to write file to external directory
         //               writeToStorage(getApplicationContext(), "StudnetJson", stdJson)
                    }

                    @Override
                    public void onError(Throwable e) {
                        // Show error message
                    }
                });
    }

}