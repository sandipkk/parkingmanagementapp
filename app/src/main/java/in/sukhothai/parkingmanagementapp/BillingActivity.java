package in.sukhothai.parkingmanagementapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDaoList;
import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingList;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;

public class BillingActivity extends AppCompatActivity {
    EditText type, perHourCharge, overNightCharges;
    String typeS, perHourChargeS, overNightChargesS;
    Button submit;
    private static BillingDataBase billingDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);
        billingDatabase = Room.databaseBuilder(BillingActivity.this, BillingDataBase.class, "BillingCollection").allowMainThreadQueries().build();

        BillingDaoList billingDataList = new BillingDaoList();
        overNightCharges = findViewById(R.id.overNightCharges);
        perHourCharge = findViewById(R.id.perHourCharge);
        submit = findViewById(R.id.submit);
        type = findViewById(R.id.type);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                perHourChargeS = perHourCharge.getText().toString();
                typeS = type.getText().toString();
                overNightChargesS = overNightCharges.getText().toString();
                billingDataList.setPerHourCharge(perHourChargeS);
                billingDataList.setType(typeS);
                billingDataList.setOverNightCharges(overNightChargesS);
                billingDatabase.billingDao().addData(billingDataList);
              /*  MainActivity.tourPager.setCurrentItem(1);
                ViewParkingVehiclesFragment.getDatafromdatabase();*/
                type.setText("");
                overNightCharges.setText("");
                perHourCharge.setText("");
                MyLog.d("ListofDATRABASE!!!!!", billingDataList.toString());
            }
        });
    }
}