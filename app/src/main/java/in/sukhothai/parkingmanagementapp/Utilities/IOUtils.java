package in.sukhothai.parkingmanagementapp.Utilities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import in.sukhothai.parkingmanagementapp.MainActivity;

/**
 * Created by kesari on 13/04/17.
 */

public class IOUtils {

    private static final String TAG = "IO_Utils";

    //Volley JSON Object Request
    public void sendCommonJSONObjectRequest(int RequestType, String API_Tag, final Context context, String url, JSONObject jsonObject, final VolleyCallback callback, final VolleyFailureCallback failureCallback) {

        final Map<String, String> paramsHeaders = new HashMap<String, String>();
        paramsHeaders.put("Authorization", "JWT " + SharedPrefUtil.getToken(context));

        MyLog.i("TOKEN#######", paramsHeaders.toString());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(RequestType,
                url, jsonObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("response", response.toString());
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                try {
                    String json = null;
                    NetworkResponse response = error.networkResponse;
                    json = new String(response.data);
                    //Log.d("Error", json);
                    failureCallback.onFailure(json);

                    error.printStackTrace();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return paramsHeaders;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        //Adding request to request queue
        MyApplication.getInstance().addRequestToQueue(jsonObjReq, API_Tag);

    }

    //Volley JSON Array Request
    public void sendCommonJSONArrayRequest(int RequestType, String API_Tag, final Context context, String url, JSONArray jsonArray, final VolleyCallback callback, final VolleyFailureCallback failureCallback) {

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                RequestType,
                url,
                jsonArray,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Do something with response
                        callback.onSuccess(String.valueOf(response));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something when error occurred
                        failureCallback.onFailure("");
                    }
                }
        );

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        //Adding request to request queue
        MyApplication.getInstance().addRequestToQueue(jsonArrayRequest, API_Tag);
    }

    // Volley String Get Request
    public void getCommonStringRequest(int RequestType, String API_Tag, final Context context, String url, final VolleyCallback callback, final VolleyFailureCallback failureCallback) {

       // final Map<String, String> paramsHeaders = new HashMap<String, String>();
       // paramsHeaders.put("Authorization", "JWT " + SharedPrefUtil.getToken(context));
        //MyLog.i("#######", paramsHeaders.toString());

        StringRequest stringRequest = new StringRequest(RequestType, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //Log.d("Response", response.toString());
                callback.onSuccess(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", error.toString());
                failureCallback.onFailure("");
            }
        }) /*{
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return paramsHeaders;
            }
        }*/;

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addRequestToQueue(stringRequest, API_Tag);
    }


    public interface VolleyFailureCallback {
        void onFailure(String result);
    }

    public interface VolleyCallback {
        void onSuccess(String result);
    }

    public static boolean isServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void buildAlertMessageNoGps(final Activity context) {

        try {

            GoogleApiClient googleApiClient = null;
            final int REQUEST_LOCATION = 199;

            if (googleApiClient == null) {
                final GoogleApiClient finalGoogleApiClient = googleApiClient;
                googleApiClient = new GoogleApiClient.Builder(context)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                            @Override
                            public void onConnected(Bundle bundle) {

                            }

                            @Override
                            public void onConnectionSuspended(int i) {
                                try {
                                    finalGoogleApiClient.connect();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(ConnectionResult connectionResult) {

                                Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                            }
                        }).build();
                googleApiClient.connect();

                LocationRequest locationRequest = LocationRequest.create();
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setInterval(30 * 1000);
                locationRequest.setFastestInterval(5 * 1000);
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest);

                builder.setAlwaysShow(true);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(LocationSettingsResult result) {
                        final Status status = result.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    // Show the dialog by calling startResolutionForResult(),
                                    // and check the result in onActivityResult().
                                    status.startResolutionForResult(context, REQUEST_LOCATION);

                                    //context.finish();
                                } catch (IntentSender.SendIntentException e) {
                                    // Ignore the error.
                                }
                                break;
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static String parseDateToddMMyyyy(String time) {

        String str = null;

        if (!time.isEmpty()) {
            String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            String outputPattern = "dd/MM/yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            outputFormat.setTimeZone(TimeZone.getDefault());

            Date date = null;
            // String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            str = "";
        }

        return str;
    }

    public static String parseDateFormat(String time, String inputPattern, String outputPattern) {

        String str = null;

        if (!time.isEmpty()) {
            //String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            outputFormat.setTimeZone(TimeZone.getDefault());

            Date date = null;
            // String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            } catch (Exception e) {
                e.printStackTrace();
                str = time;
            }
        } else {
            str = "";
        }

        return str;
    }

    public static boolean permStatus = false;

    public static boolean checkAndRequestPermissions(final Activity activity, String... permissions) {

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                //Toast.makeText(HomeScreen.this, "Permission Granted", Toast.LENGTH_SHORT).show();
                try {
                    permStatus = true;
                    final LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        buildAlertMessageNoGps(activity);
                    } else {
                        if (!IOUtils.isServiceRunning(LocationUpdatesService.class, activity)) {
                            // LOCATION SERVICE
                            activity.startService(new Intent(activity, LocationUpdatesService.class));
                            MyLog.e(TAG, "Location service is already running");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                permStatus = false;
                //Toast.makeText(activity, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };

        TedPermission.with(activity)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(permissions)
                .check();

        return permStatus;
    }

    public static String getContactDisplayNameByNumber(String number, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String name = "Incoming call from";

        ContentResolver contentResolver = context.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, null, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                // this.id =
                // contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.CONTACT_ID));
                // String contactId =
                // contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
            } else {
                name = "Unknown number";
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return name;
    }
}
