package in.sukhothai.parkingmanagementapp.Utilities;

/**
 * Created by Suraj Nirmal on 31/01/18.
 */

public interface Constants {

    int GET = 0;
    int POST = 1;
    int PUT = 2;
    int DELETE = 3;

    // Milliseconds per second
    public static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 10;
    // Update frequency in milliseconds
    public static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    public static final int FASTEST_INTERVAL_IN_SECONDS = 60;
    // A fast frequency ceiling in milliseconds
    public static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    //String StagingIP = "http://192.168.1.220:3000/route/";
    //String LoginIP = "http://192.168.1.7:5002/route/";
    String LoginIP = "http://login.sukhothai.in/route/";

    //Todo login
    String getLogin = "http://login.sukhothai.in/app/login";

    //TODO parkingList
    String parkingList = LoginIP + "parking/parkingList?vehicleNo=";

    String parkingOut = LoginIP + "parking/parkingOut";

    String parkingIn = LoginIP + "parking/parkingIn";

    String vehicleMasterList = LoginIP + "parking/vehicleMasterList";

    //TODO getAwsUploadLink
    String getAwsUploadLink = " http://login.sukhothai.in/route/upload/aws/";

    //TODO getAllBranchLink
   }

