package in.sukhothai.parkingmanagementapp.RoomDataBase;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
//172.16.5.19
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "ParkingDb")
public class ParkingList {


    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "parkingType")
    private String parkingType;

    @ColumnInfo(name = "vehicleOwner")
    private String vehicleOwner;

    @ColumnInfo(name = "mobile")
    private String mobile;

    @ColumnInfo(name = "tranNo")
    private String tranNo;

    @SerializedName("mobParkInDt")
    private String mobParkInDt;

    @SerializedName("parkingInHrMin")
    private String parkingInHrMin;

    @SerializedName("parkingOut")
    private String parkingOut;

    @SerializedName("billAmount")
    private String billAmount;

    @SerializedName("parkingHours")
    private String parkingHours;


    @SerializedName("parkingOutDate")
    private String parkingOutDate;

    @SerializedName("mobParkOutDt")
    private String mobParkOutDt;

    @SerializedName("vehicleNumber")
    private String vehicleNumber;

    @SerializedName("vehicleType")
    private String vehicleType;

    @SerializedName("parkingDays")
    private String parkingDays;

    @SerializedName("parkingMinutes")
    private String parkingMinutes;

    @SerializedName("image")
    private String image;

    @SerializedName("imageFilePathGallery")
    private String imageFilePathGallery;

    @SerializedName("exitdone")
    private String exitdone;

    @SerializedName("copytoserver")
    private String copytoserver;

    @SerializedName("deleted")
    private String deleted;

    @SerializedName("owneridimage")
    private String owneridimage;

    @SerializedName("cgst")
    private String cgst;

    @SerializedName("sgst")
    private String sgst;

    @SerializedName("srno")
    private String srno;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParkingType() {
        return parkingType;
    }

    public void setParkingType(String parkingType) {
        this.parkingType = parkingType;
    }

    public String getVehicleOwner() {
        return vehicleOwner;
    }

    public void setVehicleOwner(String vehicleOwner) {
        this.vehicleOwner = vehicleOwner;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTranNo() {
        return tranNo;
    }

    public void setTranNo(String tranNo) {
        this.tranNo = tranNo;
    }

    public String getMobParkInDt() {
        return mobParkInDt;
    }

    public void setMobParkInDt(String mobParkInDt) {
        this.mobParkInDt = mobParkInDt;
    }

    public String getParkingInHrMin() {
        return parkingInHrMin;
    }

    public void setParkingInHrMin(String parkingInHrMin) {
        this.parkingInHrMin = parkingInHrMin;
    }

    public String getParkingOut() {
        return parkingOut;
    }

    public void setParkingOut(String parkingOut) {
        this.parkingOut = parkingOut;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getParkingHours() {
        return parkingHours;
    }

    public void setParkingHours(String parkingHours) {
        this.parkingHours = parkingHours;
    }

    public String getParkingOutDate() {
        return parkingOutDate;
    }

    public void setParkingOutDate(String parkingOutDate) {
        this.parkingOutDate = parkingOutDate;
    }

    public String getMobParkOutDt() {
        return mobParkOutDt;
    }

    public void setMobParkOutDt(String mobParkOutDt) {
        this.mobParkOutDt = mobParkOutDt;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getParkingDays() {
        return parkingDays;
    }

    public void setParkingDays(String parkingDays) {
        this.parkingDays = parkingDays;
    }

    public String getParkingMinutes() {
        return parkingMinutes;
    }

    public void setParkingMinutes(String image) {
        this.parkingMinutes = image;
    }

    public String getExitdone() {
        return exitdone;
    }

    public void setExitdone(String exitdone) {
        this.exitdone = exitdone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCopytoserver() {
        return copytoserver;
    }

    public void setCopytoserver(String copytoserver) {
        this.copytoserver = copytoserver;
    }


    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getOwneridimage() {
        return owneridimage;
    }

    public void setOwneridimage(String owneridimage) {
        this.deleted = owneridimage;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        this.srno = srno;
    }

    public String getImageFilePathGallery() {
        return imageFilePathGallery;
    }

    public void setImageFilePathGallery(String imageFilePathGallery) {
        this.imageFilePathGallery = imageFilePathGallery;
    }

    @Override
    public String toString() {
        return "ParkingList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parkingType='" + parkingType + '\'' +
                ", vehicleOwner='" + vehicleOwner + '\'' +
                ", mobile='" + mobile + '\'' +
                ", tranNo='" + tranNo + '\'' +
                ", mobParkInDt='" + mobParkInDt + '\'' +
                ", parkingInHrMin='" + parkingInHrMin + '\'' +
                ", parkingOut='" + parkingOut + '\'' +
                ", billAmount='" + billAmount + '\'' +
                ", parkingHours='" + parkingHours + '\'' +
                ", parkingOutDate='" + parkingOutDate + '\'' +
                ", mobParkOutDt='" + mobParkOutDt + '\'' +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", parkingDays='" + parkingDays + '\'' +
                ", parkingMinutes='" + parkingMinutes + '\'' +
                ", image='" + image + '\'' +
                ", imageFilePathGallery='" + imageFilePathGallery + '\'' +
                ", exitdone='" + exitdone + '\'' +
                ", copytoserver='" + copytoserver + '\'' +
                ", deleted='" + deleted + '\'' +
                ", owneridimage='" + owneridimage + '\'' +
                ", cgst='" + cgst + '\'' +
                ", sgst='" + sgst + '\'' +
                ", srno='" + srno + '\'' +
                '}';
    }
}
