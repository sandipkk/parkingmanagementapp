package in.sukhothai.parkingmanagementapp.RoomDataBase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

/*
@Dao
public interface MyDao {
    @Insert
    public void addData(MyDatalist mydatalist);

    @Query("select * from mydatalist")
    public List<MyDatalist> getMyData();

    @Delete
    public void delete(MyDatalist mydatalist);

    @Update
    public void update(MyDatalist mydatalist);

    @Query("DELETE FROM mydatalist WHERE id = :id")
    void deleteById(int id);

    @Query("UPDATE mydatalist SET name =:name ,email=:email , city =:city WHERE id= id ")
    int updateUser(String name, String email, String city);
/*
    @Query("UPDATE item SET quantity = quantity + 1 WHERE id = :id")
    void updateQuantity(int id)
    */
@Dao
public interface ParkingDao {

    @Insert
    public void addData(ParkingList mydatalist);

    @Query("select * from ParkingDb")
    public List<ParkingList> getMyData();

    @Query("UPDATE ParkingDb SET parkingHours =:parkingHours ,parkingOutDate =:parkingOutDate ,mobParkOutDt =:mobParkOutDt ,billAmount =:billAmount " +
            " ,parkingDays =:parkingDays ,cgst =:cgst  , sgst =:sgst ,copytoserver=:copytoserver ,exitdone =:exitdone ,deleted =:deleted  WHERE id= :id")
    int updateParkingList(String parkingHours,String parkingOutDate , String mobParkOutDt, String billAmount,
                          String parkingDays, String cgst, String sgst, String copytoserver, String exitdone, String deleted, String id);

    @Delete
    void deleteVehicle(ParkingList Parkinglist);

    @Query("UPDATE ParkingDb SET deleted =:deleted WHERE id= :id ")
    int updateDelete(String deleted ,String id );
}