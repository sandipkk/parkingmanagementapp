package in.sukhothai.parkingmanagementapp.RoomDataBase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

/*

@Database(entities={MyDatalist.class},version = 1)
public abstract class  MyDatabase extends RoomDatabase {
    public abstract MyDao myDao();
*/

@Database(entities = {BillingDaoList.class}, version = 2)

public abstract class BillingDataBase extends RoomDatabase {
    public abstract BillingDao billingDao();
}
