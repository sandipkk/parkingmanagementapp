package in.sukhothai.parkingmanagementapp.RoomDataBase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

/*

@Dao
public interface MyDao {
    @Insert
    public void addData(MyDatalist mydatalist);

    @Query("select * from mydatalist")
    public List<MyDatalist> getMyData();

    @Delete
    public void delete(MyDatalist mydatalist);

    @Update
    public void update(MyDatalist mydatalist);

    @Query("DELETE FROM mydatalist WHERE id = :id")
    void deleteById(int id);

    @Query("UPDATE mydatalist SET name =:name ,email=:email , city =:city WHERE id= id ")
    int updateUser(String name, String email, String city);
/*
    @Query("UPDATE item SET quantity = quantity + 1 WHERE id = :id")
    void updateQuantity(int id)
    */
@Dao
public interface BillingDao {

    @Insert
    public void addData(BillingDaoList billingDaoList);

    @Query("select * from BillingCollection")
    public List<BillingDaoList> getBillingData();



/*    @Query("UPDATE ParkingDb SET parkingHours =:parkingHours ,mobParkOutDt =:mobParkOutDt ,billAmount =:billAmount  WHERE id= id ")
    int updateParkingList(String parkingHours, String mobParkOutDt, String billAmount);*/


}