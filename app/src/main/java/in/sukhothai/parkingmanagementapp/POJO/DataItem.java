package in.sukhothai.parkingmanagementapp.POJO;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("perHourCharge")
	private int perHourCharge;

	@SerializedName("overNightCharges")
	private int overNightCharges;

	@SerializedName("_id")
	private String id;

	@SerializedName("type")
	private String type;

	public int getPerHourCharge(){
		return perHourCharge;
	}

	public int getOverNightCharges(){
		return overNightCharges;
	}

	public String getId(){
		return id;
	}

	public String getType(){
		return type;
	}
}