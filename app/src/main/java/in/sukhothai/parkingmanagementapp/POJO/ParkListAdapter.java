package in.sukhothai.parkingmanagementapp.POJO;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.bumptech.glide.Glide;
import com.dantsu.escposprinter.connection.DeviceConnection;
import com.dantsu.escposprinter.connection.tcp.TcpConnection;
import com.dantsu.escposprinter.textparser.PrinterTextParserImg;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDaoList;
import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingList;
import in.sukhothai.parkingmanagementapp.ViewParkingVehiclesFragment;
import in.sukhothai.parkingmanagementapp.R;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;
import in.sukhothai.parkingmanagementapp.WorkManager.MyWorker;
import in.sukhothai.parkingmanagementapp.async.AsyncEscPosPrinter;
import in.sukhothai.parkingmanagementapp.async.AsyncTcpEscPosPrint;

import static in.sukhothai.parkingmanagementapp.ViewParkingVehiclesFragment.ctx;

public class ParkListAdapter extends RecyclerView.Adapter<ParkListAdapter.ViewHolder> {

    public static String srnOout, vehicleNumberSTRINGadpter, hours, vehicleBillgst, vehicleBill, vadpteroutdate, vadpterindate, ParkingOutDate, vGadptercgst, vadptersgst;
    private static List<ParkingList> parkingResponses;
    private List<BillingDaoList> billingPojoList;
    private static Context context;
    public String headerDateAdapter, todayout, parkinGtype, billAmount, VehicleType, getVehicleType, perHourCharge, overNightCharges,
            parkingTypefromdb, copytoserver, exitdone, today, currentTimeofout, outdateandtime, dayOfTheWeek, fulldateformat, currentTime, indateandtime, currentdate;
    private static ParkingDataBase myDatabase;
    public static String deleted;
    private static BillingDataBase billingdatabase;
    private long diffHours;
    private long diffDays;
    private double Fcgst, Fsgst;
    private List<BillingDaoList> billingdaolist;

    public ParkListAdapter(List<ParkingList> parkingResponses, List<BillingDaoList> billingPojoList, Context context) {
        this.parkingResponses = parkingResponses;
        this.billingPojoList = billingPojoList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_single_new_parking_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        billingPojoList = new ArrayList<>();
        myDatabase = Room.databaseBuilder(context, ParkingDataBase.class, "ParkingDb").allowMainThreadQueries().build();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParkListAdapter.ViewHolder holder, int position) {
        ParkingList md = parkingResponses.get(position);
        holder.DateTXT.setText(parkingResponses.get(position).getMobParkInDt());
        holder.VehicalNumberTXT.setText(parkingResponses.get(position).getVehicleNumber());
        String tranNo = String.valueOf(parkingResponses.get(position).getId());
        parkinGtype = String.valueOf(parkingResponses.get(position).getParkingType());
        if (parkingResponses.get(position).getImage() != null) {
            String imagelogo = parkingResponses.get(position).getImage();

            if (!imagelogo.equalsIgnoreCase("")) {
                Log.i("imageUrl1", imagelogo);
                Glide.with(context)
                        .load(imagelogo)
                        .into(holder.vehicleIMAGE);
            } else {
                Glide.with(context)
                        .load(R.drawable.ic_blankimage)
                        .into(holder.vehicleIMAGE);
            }
        } else {
            //holder.vehicleIMAGE.setImageResource(R.drawable.ic_blankimage);
            Glide.with(context)
                    .load(R.drawable.ic_blankimage)
                    .into(holder.vehicleIMAGE);
        }

        try {
            if (parkingResponses.get(position).getMobParkOutDt() != null) {
                holder.mobParkOutDt.setText(parkingResponses.get(position).getMobParkOutDt());
                holder.vehicalPriceTV.setText(context.getResources().getString(R.string.rs) + String.valueOf(parkingResponses.get(position).getBillAmount()));
                holder.vehicalHoursTV.setText(String.valueOf(parkingResponses.get(position).getParkingHours()));
                holder.vehicalPriceTV.setVisibility(View.VISIBLE);
                holder.vehicalHoursTV.setVisibility(View.VISIBLE);
                holder.ouTdateholder.setVisibility(View.VISIBLE);
                holder.ouTHolder.setVisibility(View.GONE);
            } else {
                holder.vehicalPriceTV.setVisibility(View.GONE);
                holder.hours.setVisibility(View.GONE);
                holder.vehicalHoursTV.setVisibility(View.GONE);
                holder.ouTdateholder.setVisibility(View.GONE);
                holder.ouTHolder.setVisibility(View.VISIBLE);
            }// Per Hour

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.ouTHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getdate();
                    getoutdate(position);
                    exitdone = "yes";
                    deleted = "no";
                    copytoserver = "no";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return parkingResponses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView VehicalNumberTXT, DateTXT, mobParkOutDt, vehicalPriceTV, vehicalHoursTV, IntimeTV, mobParkOutTime, hours;
        LinearLayout ouTdateholder, ouTHolder;
        ImageView vehicleIMAGE;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);
            VehicalNumberTXT = (TextView) itemView.findViewById(R.id.VehicalNumberTXT);
            DateTXT = (TextView) itemView.findViewById(R.id.DateTXT);
            mobParkOutDt = (TextView) itemView.findViewById(R.id.mobParkOutDt);
            vehicalPriceTV = (TextView) itemView.findViewById(R.id.vehicalPriceTV);
            ouTdateholder = (LinearLayout) itemView.findViewById(R.id.ouTdateholder);
            ouTHolder = (LinearLayout) itemView.findViewById(R.id.ouTHolder);
            vehicalHoursTV = (TextView) itemView.findViewById(R.id.vehicalHoursTV);
            IntimeTV = (TextView) itemView.findViewById(R.id.IntimeTV);
            mobParkOutTime = (TextView) itemView.findViewById(R.id.mobParkOutTime);
            vehicleIMAGE = (ImageView) itemView.findViewById(R.id.vehicleIMAGE);
            hours = (TextView) itemView.findViewById(R.id.hours);
        }
    }

    public void getoutdate(int position) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            String curent = (parkingResponses.get(position).getMobParkInDt());
            VehicleType = (parkingResponses.get(position).getVehicleType());
            vehicleNumberSTRINGadpter = parkingResponses.get(position).getVehicleNumber();
            SimpleDateFormat formatternew = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
            try {
                Date currentDate = new Date();
                Date date = Calendar.getInstance().getTime();
                todayout = formatternew.format(date);
                Date datew1 = formatternew.parse(curent);
                if (parkinGtype.equalsIgnoreCase("Over Night")) {
                    long getHoursOverNight = getHoursOverNight(datew1, currentDate);
                    //  getDataBillingCollectionOverNight(getHoursOverNight, position);
                    getDataBillingCollection(getHoursOverNight, position);
                    //            printRecieptDataoutOvernight();
                } else if (parkinGtype.equalsIgnoreCase("Per Hour")) {
                    long getHour = getHours(datew1, currentDate);
                    getDataBillingCollection(getHour, position);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long getHours(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long perhours = diffHours;
        return diffHours;
    }

    private long getHoursOverNight(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        diffDays = diff / (24 * 60 * 60 * 1000);
        diffHours = diff / (60 * 60 * 1000) % 24;
        return diffDays;
    }

    public void updateVehicalList(int position) {
        ParkingList md = parkingResponses.get(position);
        md.setMobParkOutDt(todayout);
        md.setParkingHours(String.valueOf(diffHours));
        md.setSgst(String.valueOf(Fsgst));
        md.setCgst(String.valueOf(Fcgst));
        md.setBillAmount(billAmount);
        md.setParkingOutDate(headerDateAdapter);
        md.setExitdone(exitdone);
        md.setDeleted(deleted);
        md.setCopytoserver(copytoserver);
        MyLog.e("String.valueOf(md.getId())", String.valueOf(md.getId()));
        myDatabase.parkingDao().updateParkingList(String.valueOf(diffHours), headerDateAdapter, todayout, billAmount, String.valueOf(diffDays), String.valueOf(Fcgst), String.valueOf(Fsgst), copytoserver, exitdone, deleted, String.valueOf(md.getId()));
        ViewParkingVehiclesFragment.getDatafromdatabase();
        MyLog.i("UpdatedlistwhenUpdated", parkingResponses.toString());
        vehicleNumberSTRINGadpter = parkingResponses.get(position).getVehicleNumber();
        MyLog.e("parkingposition", parkingResponses.get(position).toString());
        srnOout = parkingResponses.get(position).getSrno();
        vehicleBillgst = parkingResponses.get(position).getBillAmount();
        vadpteroutdate = parkingResponses.get(position).getMobParkOutDt();
        vadpterindate = parkingResponses.get(position).getParkingInHrMin();
        hours = parkingResponses.get(position).getParkingHours();
        vGadptercgst = parkingResponses.get(position).getCgst();
        vadptersgst = parkingResponses.get(position).getSgst();
        String intime = parkingResponses.get(position).getMobParkInDt();
        parkingTypefromdb = parkingResponses.get(position).getParkingType();
    }

    public void delete(int position) {
        try {

            deleted = "yes";
            ParkingList md = parkingResponses.get(position);
            md.setDeleted(deleted);
            myDatabase.parkingDao().updateDelete(deleted, String.valueOf(md.getId()));
            MyLog.e("IFDELETED@@", parkingResponses.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getDataBillingCollection(long timeandday, int position) {
        try {
            billingdatabase = Room.databaseBuilder(context, BillingDataBase.class, "BillingCollection").allowMainThreadQueries().build();
            class GetData extends AsyncTask<Void, Void, List<BillingDaoList>> {
                @Override
                protected List<BillingDaoList> doInBackground(Void... voids) {
                    billingdaolist = billingdatabase.billingDao().getBillingData();
                    MyLog.d("BillingDaoList", billingdaolist.toString());
                    return billingdaolist;
                }

                @SuppressLint("DefaultLocale")
                @Override
                protected void onPostExecute(List<BillingDaoList> myDataList) {
                    try {
                        Log.i("BillingDaoList", billingdaolist.toString());
                        MyLog.e("parkinGtype ", parkinGtype);
                        for (int q = 0; q < billingdaolist.size(); q++) {
                            getVehicleType = billingdaolist.get(q).getType();
                            perHourCharge = billingdaolist.get(q).getPerHourCharge();
                            overNightCharges = billingdaolist.get(q).getOverNightCharges();
                            if (getVehicleType.equalsIgnoreCase(VehicleType)) {

                                if (parkinGtype.equalsIgnoreCase("Over Night")) {
                                    if (timeandday == 0) {
                                        diffDays = 1;
                                    }
                                    if (timeandday == 0) {
                                        diffHours = 1;
                                    }
                                    billAmount = String.valueOf(diffDays * Integer.parseInt(overNightCharges));

                                    MyLog.e("OverbillAmount Night", overNightCharges);
                                } else {
                                    if (timeandday == 0) {
                                        diffHours = 1;
                                    }
                                    billAmount = String.valueOf(diffHours * Integer.parseInt(perHourCharge));
                                    MyLog.e("OverbillAmount Hour", perHourCharge);
                                }
                            } else {
                                Log.i("not match", "not match");
                            }
                            try {
                                Log.i("matches", "try catch");
                                double cgst = Double.parseDouble(String.valueOf(Double.parseDouble(billAmount) * 0.09));
                                double sgst = Double.parseDouble(String.valueOf(Double.parseDouble(billAmount) * 0.09));

                                Fsgst = Double.parseDouble(new DecimalFormat("##.####").format(sgst));
                                Fcgst = Double.parseDouble(new DecimalFormat("##.####").format(cgst));
                                Log.i("sgst", String.valueOf(Fsgst));
                                Log.i("cgst", String.valueOf(Fcgst));
                                double gstamount = Fcgst + Fsgst;
                                vehicleBill = String.valueOf(Integer.parseInt(billAmount) - gstamount);
                                updateVehicalList(position);
                                OneTimeWorkRequest mywork =
                                        new OneTimeWorkRequest.Builder(MyWorker.class)
                                                .setInitialDelay(1, TimeUnit.MINUTES)// Use this when you want to add initial delay or schedule initial work to `OneTimeWorkRequest` e.g. setInitialDelay(2, TimeUnit.HOURS)
                                                .build();
                                //       delete(position);
                                WorkManager.getInstance().enqueue(mywork);
                                if (parkinGtype.equalsIgnoreCase("Over Night")) {
                                    printRecieptDataoutOvernight();
                                } else {
                                    printRecieptDataout();
                                }
                                Log.i("vehicleBill!!", String.valueOf(vehicleBill));
                                ViewParkingVehiclesFragment.getDatafromdatabase();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    super.onPostExecute(myDataList);
                }
            }
            GetData gd = new GetData();
            gd.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*
    public void getDataBillingCollectionOverNight(long days, int position) {
        try {
            billingdatabase = Room.databaseBuilder(context, BillingDataBase.class, "BillingCollection").allowMainThreadQueries().build();
            class GetData extends AsyncTask<Void, Void, List<BillingDaoList>> {

                @Override
                protected List<BillingDaoList> doInBackground(Void... voids) {
                    billingdaolist = billingdatabase.billingDao().getBillingData();
                    MyLog.d("BillingDaoList", billingdaolist.toString());
                    return billingdaolist;
                }


                @SuppressLint("DefaultLocale")
                @Override
                protected void onPostExecute(List<BillingDaoList> myDataList) {
                    try {
                        Log.i("BillingDaoList", billingdaolist.toString());
                        for (int q = 0; q < billingdaolist.size(); q++) {
                            getVehicleType = billingdaolist.get(q).getType();
                            perHourCharge = billingdaolist.get(q).getPerHourCharge();
                            overNightCharges = billingdaolist.get(q).getOverNightCharges();
                            if (getVehicleType.equalsIgnoreCase(VehicleType)) {
                                Log.i("matches", "gethoursandpriceOVERNIGHT");
                                if (days == 0) {
                                    diffDays = 1;
                                }
                                if (diffHours == 0) {
                                    diffHours = 1;
                                }
                                billAmount = String.valueOf(diffDays * Integer.parseInt(overNightCharges));
                                double cgst = (Integer.parseInt(billAmount) * 0.09);
                                double sgst = (Integer.parseInt(billAmount) * 0.09);
                          */
/*      Fsgst = Double.parseDouble((new DecimalFormat("%.2f").format(Fsgst)));
                                Fcgst = Double.parseDouble((new DecimalFormat("%.2f").format(Fcgst)));
*//*

                                Fsgst = Double.parseDouble(new DecimalFormat("##.####").format(sgst));
                                Fcgst = Double.parseDouble(new DecimalFormat("##.####").format(cgst));
                                double gstamount = Fcgst + Fsgst;
                                vehicleBill = String.valueOf(Integer.parseInt(billAmount) - gstamount);
                                Log.i(" vehicleBill", vehicleBill);
                            //    updateVehicalList(position);
                                //    printRecieptDataoutOvernight();
                                ViewParkingVehiclesFragment.getDatafromdatabase();
                            } else {
                                Toast.makeText(context, "NOt match", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    super.onPostExecute(myDataList);
                }
            }
            GetData gd = new GetData();
            gd.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/

    public void printRecieptDataout() {
        try {
            try {
                new AsyncTcpEscPosPrint(ctx)
                        .execute(getAsyncEscPosPrinterOutww(new TcpConnection("172.16.5.16", 9100)));
            } catch (NumberFormatException e) {
                new androidx.appcompat.app.AlertDialog.Builder(ctx)
                        .setTitle("Invalid TCP port address")
                        .setMessage("Port field must be a number.")
                        .show();
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printRecieptDataoutOvernight() {
        try {
            try {
                new AsyncTcpEscPosPrint(ctx)
                        .execute(getAsyncEscPosPrinterOutOvernight(new TcpConnection("172.16.5.16", 9100)));
            } catch (NumberFormatException e) {
                new androidx.appcompat.app.AlertDialog.Builder(ctx)
                        .setTitle("Invalid TCP port address")
                        .setMessage("Port field must be a number.")
                        .show();
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AsyncEscPosPrinter getAsyncEscPosPrinterOutww(DeviceConnection printerConnection) {
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 203, 77f, 45);
        return printer.setTextToPrint(
                "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, ctx.getResources().getDrawableForDensity(R.drawable.newlogo, DisplayMetrics.DENSITY_MEDIUM)) + "</img>\n\n" +
                        "[L]" + headerDateAdapter + "[R][C]" + "SR#" + srnOout + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[C]<font size='tall'>  Vehicle Number :" + vehicleNumberSTRINGadpter + "</font>\n" +
                        "[C]\n" +
                        "[L]ENTRY[C][C]EXIT[R][C]HOURS\n" +
                        "[L]<font size='tall'>" + vadpterindate + "</font>[C][C]<font size='tall'>" + currentTime + "</font>[R][C]<font size='tall'>" + hours + "</font>\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]Parking Charges[R]Rs." + vehicleBill + "\n" +
                        "[L]C.G.S.T @ 9%[R]Rs. " + vGadptercgst + "\n" +
                        "[L]S.G.S.T @ 9%[R]Rs. " + vadptersgst + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<b><font size='big'>Total[R] Rs." + vehicleBillgst + "</font></b>\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='normal'>Sukhothai India Pvt.Ltd. <b>GST # 27AAOCS623OP2ZM</b></font>\n" +
                        "[C]<b><font size='normal'>Terms of Parking</b>\n" +
                        "[L]* Parking will be at owner's risk.\n" +
                        "* No responsibility of any loss of valuables.\n" +
                        "* No responsibility for any vehicle damage by     other vehicles or by accident.\n" +
                        "* Any chargable timing above an hour will be      consider as next hour.\n" +
                        "* Operating Hours 7 AM - 11 PM'\n</font>\n" +
                        //   "[C]<barcode type='ean8' height='10' text='none'>" + "srno" + "</barcode>\n" +
                        //   "[C]<barcode type='ean8' height='25' width='50' text='none'>"+srno+"</barcode>\n"+
                        "[C]<font size='normal'>Thank You</font>\n" +
                        "[C]\n" +
                        "[C]\n"
        );
    }

    public AsyncEscPosPrinter getAsyncEscPosPrinterOutOvernight(DeviceConnection printerConnection) {
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 203, 77f, 45);
        return printer.setTextToPrint(
                "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, ctx.getResources().getDrawableForDensity(R.drawable.newlogo, DisplayMetrics.DENSITY_MEDIUM)) + "</img>\n\n" +
                        "[L]" + headerDateAdapter + "[R][C]" + "SR#" + srnOout + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[C]<font size='tall'>  Vehicle Number :" + vehicleNumberSTRINGadpter + "</font>\n" +
                        "[C]\n" +
                        "[L]ENTRY[C][C]EXIT[R][C]HOURS\n" +
                        "[L]<font size='tall'>" + vadpterindate + "</font>[C][C]<font size='tall'>" + currentTime + "</font>[R][C]<font size='tall'>" + hours + "</font>\n" +
                        "[C](Night Parking)\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]Parking Charges[R]Rs." + vehicleBill + "\n" +
                        "[L]C.G.S.T @ 9%[R]Rs. " + vGadptercgst + "\n" +
                        "[L]S.G.S.T @ 9%[R]Rs. " + vadptersgst + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<b><font size='big'>Total[R] Rs." + vehicleBillgst + "</font></b>\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='normal'>Sukhothai India Pvt.Ltd. <b>GST # 27AAOCS623OP2ZM</b></font>\n" +
                        "[C]<b><font size='normal'>Terms of Parking</b>\n" +
                        "[L]* Parking will be at owner's risk.\n" +
                        "* No responsibility of any loss of valuables.\n" +
                        "* No responsibility for any vehicle damage by     other vehicles or by accident.\n" +
                        "* Any chargable timing above an hour will be      consider as next hour.\n" +
                        "* Operating Hours 7 AM - 11 PM'\n</font>\n" +
                        //   "[C]<barcode type='ean8' height='10' text='none'>" + "srno" + "</barcode>\n" +
                        //   "[C]<barcode type='ean8' height='25' width='50' text='none'>"+srno+"</barcode>\n"+
                        "[C]<font size='normal'>Thank You</font>\n" +
                        "[C]\n" +
                        "[C]\n"
        );
    }

    ///printer
  /*  public void getdate() {
        //for display date in 24hrs
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatter2 = new SimpleDateFormat("MMM d , yyyy");
        today = formatter.format(date);
        String today2 = formatter2.format(date);
        System.out.println("Today : " + today);
        System.out.println("Today : " + today2);
        Calendar c = Calendar.getInstance();
        int Hr24 = c.get(Calendar.HOUR_OF_DAY);
        int FS = c.get(Calendar.AM_PM);
        int Min = c.get(Calendar.MINUTE);
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:a");
        currentTimeofout = df.format(date.getTime());
        Log.e("currentTime", currentTimeofout.toString());
        outdateandtime = today + "  " + currentTimeofout;
        Log.e("indateandtime", String.valueOf(outdateandtime));
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        Log.e("indateandtime", String.valueOf(dayOfTheWeek));
        fulldateformat = dayOfTheWeek + "," + today;
        headerDate = today2 + " " + currentTimeofout;

    }*/

    public void getdate() {
        //for display date in 24hrs
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatter2 = new SimpleDateFormat("MMM d , yyyy");
        today = formatter.format(date);
        String today2 = formatter2.format(date);
        System.out.println("Today : " + today);
        System.out.println("Today : " + today2);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm a");
        currentTime = df.format(date.getTime());
        Log.e("currentTime", currentTime.toString());
        indateandtime = today + "  " + currentTime;
        Log.e("indateandtimeAADD", String.valueOf(indateandtime));
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        fulldateformat = dayOfTheWeek + "," + today;
        headerDateAdapter = today2 + " " + currentTime;
        Date currentDate = new Date();
        Log.e("currentDate", currentDate.toString());
        currentdate = currentDate.toString();
        Log.e("headerDateAdapter", headerDateAdapter.toString());
    }

    public static int deleteFileFromMediaStore(final ContentResolver contentResolver, final File file) {
        String canonicalPath;
        try {
            canonicalPath = file.getCanonicalPath();
        } catch (IOException e) {
            canonicalPath = file.getAbsolutePath();
        }
        final Uri uri = MediaStore.Files.getContentUri("external");
        final int result = contentResolver.delete(uri,
                MediaStore.Files.FileColumns.DATA + "=?", new String[]{canonicalPath});
        if (result == 0) {
            final String absolutePath = file.getAbsolutePath();
            if (!absolutePath.equals(canonicalPath)) {
                int deletedRow = contentResolver.delete(uri,
                        MediaStore.Files.FileColumns.DATA + "=?", new String[]{absolutePath});
                return deletedRow;
            }
        } else return result;
        return result;
    }

    public static void deletevehicleDATA(int position) {
        try {
            //   myDatabase.parkingDao().deleteVehicle(parkingResponses.get(position));
            String databasepath = parkingResponses.get(position).getImageFilePathGallery();
            if (databasepath != null) {
                MyLog.e("databasepath", databasepath);
                File fdelete = new File(databasepath);
                if (fdelete.exists()) {
                    if (fdelete.delete()) {
                        System.out.println("fileDeleted :" + databasepath);
                        ViewParkingVehiclesFragment.getDatafromdatabase();
                    } else {
                        System.out.println("file not Deleted :" + databasepath);
                    }
                }
                deleteFileFromMediaStore(context.getContentResolver(), fdelete);
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getdeletdList() {
        MyLog.e("deletdlistr", parkingResponses.toString());
    }
}


