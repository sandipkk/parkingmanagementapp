package in.sukhothai.parkingmanagementapp.POJO;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BillingVehiclePojo{

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("status")
	private boolean status;

	public List<DataItem> getData(){
		return data;
	}

	public boolean isStatus(){
		return status;
	}
}