package in.sukhothai.parkingmanagementapp.POJO;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.sukhothai.parkingmanagementapp.R;


/**
 * Created by Suraj Nirmal on 5/1/16.
 */
public class VehicleCategoryRecyclerAdapter extends RecyclerView.Adapter<VehicleCategoryRecyclerAdapter.RecyclerViewHolder> {
    public static int selected_position = -1;
    public ArrayList<String> VehicleTypeList = new ArrayList<>();
    private Context ctx;
    private ArrayList<DataItem> items;

    public VehicleCategoryRecyclerAdapter(ArrayList<String> VehicleTypeList, Context ctx) {
        this.VehicleTypeList = new ArrayList<>(VehicleTypeList);
        this.ctx = ctx;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_vehicle, parent, false);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);

        return recyclerViewHolder;
    }

    @SuppressLint({"ObsoleteSdkInt", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        try {

            if (selected_position == position) {

                //    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_light_golden));
                holder.categoryTV.setTextColor(ContextCompat.getColor(ctx, R.color.white));
             //   holder.vehicleImg.setColorFilter(ContextCompat.getColor(ctx, R.color.white));
                holder.vehicleImg.setColorFilter(Color.argb(255, 255, 255, 255));
            } else {

                //     holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_dark));
                holder.categoryTV.setTextColor(ContextCompat.getColor(ctx, R.color.black));
                holder.vehicleImg.setColorFilter(ContextCompat.getColor(ctx, R.color.black));
            }

// [Auto, Bus, Car, Cycle, Mini Bus, SUV/XUV, Tempo, Two Wheeler]
            holder.categoryTV.setText(VehicleTypeList.get(position).toString());
            String vehiclename = VehicleTypeList.get(position);
            if (VehicleTypeList.get(position).toString().equals("Auto")) {
                holder.vehicleImg.setImageResource(R.drawable.ic_tempo);
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_dark));

            } else if (VehicleTypeList.get(position).toString().equals("Bus")) {
                holder.vehicleImg.setImageResource(R.drawable.ic_bus);
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_blue));
            } else if (VehicleTypeList.get(position).toString().equals("Car")) {
                holder.vehicleImg.setImageResource(R.drawable.ic_car);
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_green));
            } else if (VehicleTypeList.get(position).toString().equals("Cycle")) {
                holder.vehicleImg.setImageResource(R.drawable.ic_cycle);
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_gray));
            } else if (VehicleTypeList.get(position).toString().equals("Mini Bus")) {
                holder.vehicleImg.setImageResource(R.drawable.ic_bus);
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_purple));
            } else if (VehicleTypeList.get(position).toString().equals("SUV")) {
                holder.vehicleImg.setImageResource(R.drawable.ic_car);
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_aquagreen));
            } else if (VehicleTypeList.get(position).toString().equals("Tempo")) {
                holder.vehicleImg.setImageResource(R.drawable.ic_tempo);
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_offwhite));
            } else if (VehicleTypeList.get(position).toString().equals("2 Wheeler")) {
                holder.vehicleImg.setImageResource(R.drawable.ic_bike);
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_red));
            } else {
                holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_light_golden));
            }

        /*    switch (vehiclename) {
                case "Auto":
                    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_dark));
                case "Bus":
                    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_blue));
                case "Car":
                    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_green));
                case "Cycle":
                    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_gray));
                case "Mini Bus":
                    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_aquablue));
                case "SUV/XUV":
                    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_aquagreen));
                case "Tempo":
                    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_offwhite));
                case "Bus":
                    holder.itemView.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_blue));
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return VehicleTypeList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView categoryTV;
        ImageView vehicleImg;

        public RecyclerViewHolder(View view) {
            super(view);

            categoryTV = (TextView) view.findViewById(R.id.categoryTV);
            vehicleImg = (ImageView) view.findViewById(R.id.vehicleImg);
        }
    }
}
