package in.sukhothai.parkingmanagementapp.VideoDemo;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import in.sukhothai.parkingmanagementapp.R;


public class VideoActivity extends AppCompatActivity {
 private ProgressDialog progressBar ;
 private VideoView videoView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        videoView = findViewById(R.id.videoView);
        progressBar = new ProgressDialog(VideoActivity.this);
        progressBar.setTitle("Loading Video");
        progressBar.setMessage("Please Wait");
        progressBar.setIndeterminate(false);
        progressBar.setCancelable(false);
        progressBar.show();
        try {
            MediaController mediaController =  new MediaController(this);
            mediaController.setAnchorView(videoView);
        //    Uri video = Uri.parse("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
            Uri video = Uri.parse("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4");
            videoView.setMediaController(mediaController);
            videoView.setVideoURI(video);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error",e.getMessage());
        }
        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                 VideoActivity.this.progressBar.dismiss();
                    VideoActivity.this.videoView.start();
            }
        });
 videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
     @Override
     public void onCompletion(MediaPlayer mp) {
         finish();
     }
 });
    }
}