package in.sukhothai.parkingmanagementapp.WorkManager;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import in.sukhothai.parkingmanagementapp.POJO.ParkListAdapter;
import in.sukhothai.parkingmanagementapp.ViewParkingVehiclesFragment;

import static androidx.work.ListenableWorker.Result.*;

public class MyWorker extends Worker {
    int position;

    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        ParkListAdapter.deletevehicleDATA(position);
        ViewParkingVehiclesFragment.getDatafromdatabase();

        return Result.success();
    }

}